//
//  AppDelegate.h
//  ss
//
//  Created by seedman on 2016/11/16.
//  Copyright © 2016年 seedman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

